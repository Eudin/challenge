package com.eduardomc94.challenge.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/calculator")
public class CalculatorController {

    @Autowired
    private CalculatorRepository rep;

    @GetMapping
    public String instructions() {

       return "Welcome to the calculator service!<br>" +
               "Modify the url to select the operation:<br>" +
               "Sum = http://localhost:8080/calculator/sum/<br>" +
               "Subtraction = http://localhost:8080/calculator/sub/<br>" +
               "Multiply = http://localhost:8080/calculator/multiply/<br>" +
               "Division = http://localhost:8080/calculator/division/<br>" +
               "Power = http://localhost:8080/calculator/pow/" +
               "<p>Operations history = http://localhost:8080/calculator/history/";
    }

    @GetMapping(value = "/sum")
    public String sumMenu() {
        return "Welcome to the sum function of the calculator.<br>" +
                "After the link, put a '/', the first number, another '/' and then the second number.<br>" +
                "Usage example: <br>http://localhost:8080/calculator/sum/1/2";
    }

    @GetMapping(value = "/sum/{x}/{y}")
    public String sum(@PathVariable("x") double x, @PathVariable("y") double y) {
        Calculator c = new Calculator();
        c.setX(x);
        c.setY(y);
        c.setResult(x + y);
        c.setOperation(x + " + " + y + " = " + c.getResult());
        rep.save(c);

        return c.getOperation();
    }

    @GetMapping(value = "/sub")
    public String subMenu() {
        return "Welcome to the subtraction function of the calculator.<br>" +
                "After the link, put a '/', the first number, another '/' and then the second number.<br>" +
                "Usage example: <br>http://localhost:8080/calculator/sub/2/1";
    }

    @GetMapping(value = "/sub/{x}/{y}")
    public String sub(@PathVariable("x") double x, @PathVariable("y") double y) {
        Calculator c = new Calculator();
        c.setX(x);
        c.setY(y);
        c.setResult(x - y);
        c.setOperation(x + " - " + y + " = " + c.getResult());
        rep.save(c);

        return c.getOperation();
    }

    @GetMapping(value = "/multiply")
    public String multiplyMenu() {
        return "Welcome to the multiply function of the calculator.<br>" +
                "After the link, put a '/', the first number, another '/' and then the second number.<br>" +
                "Usage example: <br>http://localhost:8080/calculator/multiply/5/10";
    }

    @GetMapping(value = "/multiply/{x}/{y}")
    public String multiply(@PathVariable("x") double x, @PathVariable("y") double y) {
        Calculator c = new Calculator();
        c.setX(x);
        c.setY(y);
        c.setResult(x * y);
        c.setOperation(x + " X " + y + " = " + c.getResult());
        rep.save(c);

        return String.format(c.getOperation() + "<br>Rounded: %.2f", c.getResult());
    }

    @GetMapping(value = "/division")
    public String divisionMenu() {
        return "Welcome to the division function of the calculator.<br>" +
                "After the link, put a '/', the first number, another '/' and then the second number.<br>" +
                "Usage example: <br>http://localhost:8080/calculator/division/10/5";
    }

    @GetMapping(value = "/division/{x}/{y}")
    public String division(@PathVariable("x") double x, @PathVariable("y") double y) {
        Calculator c = new Calculator();
        c.setX(x);
        c.setY(y);
        c.setResult(x / y);
        c.setOperation(x + " ÷ " + y + " = " + c.getResult());
        rep.save(c);

        return String.format(c.getOperation() + "<br>Rounded: %.1f", c.getResult());
    }

    @GetMapping(value = "/pow")
    public String powMenu() {
        return "Welcome to the power function of the calculator.<br>" +
                "After the link, put a '/', the first number, another '/' and then the second number.<br>" +
                "Usage example: <br>http://localhost:8080/calculator/pow/2/3";
    }

    @GetMapping(value = "/pow/{x}/{y}")
    public String pow(@PathVariable("x") double x, @PathVariable("y") double y) {
        Calculator c = new Calculator();
        c.setX(x);
        c.setY(y);
        c.setResult(Math.pow(x, y));
        c.setOperation(x + " POW " + y + " = " + c.getResult());
        rep.save(c);

        return String.format(c.getOperation() + "<br>Rounded: %.2f", c.getResult());
    }

    @GetMapping(value = "/history")
    public String history() {
        List<Calculator> list = rep.findAll().stream().collect(Collectors.toList());
        String history = "";
        for (Calculator c : list) {
            history += c.getOperation() + "<br>";
        }
        return history + "<p>To clear history, add '/clear' to the url.";
    }

    @GetMapping(value = "/history/clear")
    public String clearHistory() {
        rep.deleteAll();
        return "Calculator history deleted!";
    }
}

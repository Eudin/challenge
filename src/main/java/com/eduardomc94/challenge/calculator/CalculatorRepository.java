package com.eduardomc94.challenge.calculator;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CalculatorRepository extends CrudRepository<Calculator, Integer> {

    @Override
    List<Calculator> findAll();
}
